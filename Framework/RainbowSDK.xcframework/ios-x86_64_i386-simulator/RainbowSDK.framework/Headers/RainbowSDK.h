//
//  RainbowSDK.h
//  RainbowSDK
//
//  Created by Vasilis Gkanis on 23/1/20.
//  Copyright © 2020 i-SmartSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for RainbowLibrary.
FOUNDATION_EXPORT double RainbowLibraryVersionNumber;

//! Project version string for RainbowLibrary.
FOUNDATION_EXPORT const unsigned char RainbowLibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RainbowLibrary/PublicHeader.h>

#import "Rainbow.h"
#import "RUtilities.h"
#import "RCommunication.h"
#import "RItem.h"
#import "RAccount.h"
#import "RQuota.h"
#import "RTransfer.h"
#import "RTransferHelper.h"
#import "RTransferItem.h"
#import "RTransferProgress.h"

// Services
#import "RBoxService.h"
#import "RDropBoxService.h"
#import "RGoogleDriveService.h"
#import "RIDriveService.h"
#import "RMediaFireService.h"
#import "ROneDriveService.h"
#import "RSkyDriveService.h"
#import "RShareFileService.h"
#import "RSugarSyncService.h"

// API
#import "RBoxAPI.h"
#import "RDropBoxAPI.h"
//#import "RGoogleDriveAPI.h"
#import "RIDriveAPI.h"
#import "RMediaFireAPI.h"
#import "ROneDriveAPI.h"
#import "RSkyDriveAPI.h"
#import "RShareFileAPI.h"
#import "RSugarSyncAPI.h"

// Managers
#import "RBoxManager.h"
#import "RDropBoxManager.h"
#import "RGoogleDriveManager.h"
#import "RIDriveManager.h"
#import "RMediaFireManager.h"
#import "ROneDriveManager.h"
#import "RSkyDriveAPI.h"
#import "RShareFileManager.h"
#import "RSugarSyncManager.h"



