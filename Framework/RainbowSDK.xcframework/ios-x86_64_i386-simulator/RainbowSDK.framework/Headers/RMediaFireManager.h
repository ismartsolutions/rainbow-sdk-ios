#import "SBJsonParser.h"
#import "RSugarSyncManager.h"

@class RMediaFireServiceItem;

typedef enum
{
    sGetFolders=20,
    sGetFileLink,
    sSetUploadOptions,
}ssActionsMediaFile;

@interface RMediaFireManager : RSugarSyncManager<NSCopying>
{
    RMediaFireServiceItem *_rMediaFireServiceItem;
    
    int workingFileIndex;
}
@property (nonatomic, retain) RMediaFireServiceItem *keepGCPbj;
@property (nonatomic, retain)NSMutableArray *_folders,*_files;
@property (nonatomic, retain) NSMutableDictionary *quota;
@property (nonatomic, retain) NSString *searchKey;
@end
