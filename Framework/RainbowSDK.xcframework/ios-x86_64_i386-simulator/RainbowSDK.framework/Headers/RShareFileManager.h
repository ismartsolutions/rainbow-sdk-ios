#import "RSugarSyncManager.h"

@class RShareFileServiceItem;

@interface RShareFileManager : RSugarSyncManager<NSCopying>
{
    NSString  *shareFileAccessToken, *shareFileRefreshToken;

    RShareFileServiceItem *_rShareFileServiceItem;
    
    NSString *baseURL;
}
@property (nonatomic, retain) NSString *homeFolderID;

@end
