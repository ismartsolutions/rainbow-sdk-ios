#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#endif
#import "RKeys.h"
#import "REnums.h"

@class RManager;
@class RAPI;

@interface RService : NSObject<NSCoding, NSCopying>
{
    
}
@property (nonatomic, retain) NSMutableArray *list, *connectedList;
@property (nonatomic, retain) NSString *name;
@property BOOL connected;
@property ServiceLoginType loginType;
-(void)checkStatus;

-(void)storeServiceWithName:(NSString*)name;

-(RAPI*)getAPI;

@end

@interface RServiceItem : NSObject<NSCoding, NSCopying>
{
    
}
@property (nonatomic, retain) NSString *name, *iD;
@property BOOL connected;
@property BOOL keepConnected;
@property (nonatomic, retain) NSString *credentialFilename;
@property (nonatomic) void (^onStatusChecked)(BOOL success) ;
@property long long usedSpace, totalSpace;
@property (nonatomic, retain) RService *rService;

-(NSDictionary*)returnCredentials;

-(void)disconnect;
-(void)connect;
-(void)deleteCredential;
-(void)checkStatus;

-(void)statusIsNotValid;
-(void)statusIsValid;

-(RManager*)returnManager;
@end
