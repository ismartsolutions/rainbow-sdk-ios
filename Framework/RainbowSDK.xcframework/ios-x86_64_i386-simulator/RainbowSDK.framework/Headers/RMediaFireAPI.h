
#import "RAPI.h"
#import "RCommunication.h"

@class RMediaFireService;
@class RMediaFireServiceItem;

@interface RMediaFireAPI : RAPI<RCommunicationDelegate>
{
    
}
-(id)initWithServiceItem:(RMediaFireServiceItem *)ss andService:(RMediaFireService*)gBO;
@end
