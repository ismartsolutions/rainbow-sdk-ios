#import <Foundation/Foundation.h>

@class RAPI;
@class RService;
@class RServiceItem;
@class RCommunication;

@protocol RAPIDelegate <NSObject>

-(void)APILoginWasSuccessful:(RServiceItem*)serviceItem forAPI:(RAPI*)gAPI;
-(void)APISomethingWentWrong:(RServiceItem*)serviceItem forAPI:(RAPI*)gAPI errorMessage:(NSString*)msg;
@end


@interface RAPI : NSObject
{
    id delegate;

    RService *rService;
    RServiceItem *workingObj;
}

@property (nonatomic, retain) NSString *email, *password, *name;
@property (nonatomic, retain) RCommunication *connectCon;
@property (nonatomic, assign) id<RAPIDelegate>delegate;
@property (copy) void (^onStatusChecked)(RAPI *gAPI, BOOL success) ;

-(id)initWithService:(RService*)service;

-(NSString*)returnServiceName;
-(void)checkServices;
-(void)noNetworkMessage;

-(NSString*)escapeString:(NSString*)input;

-(void)storeServices;

-(RService*)returnService;
@end
