#import <Foundation/Foundation.h>
#import "XMLParser.h"
#import "RManager.h"
#import "RCommunication.h"

@class RSugarSyncServiceItem;

@interface RSugarSyncManager : RManager<RCommunicationDelegate,NSCopying>
{
    BOOL getDataForRootFolder;
    
    NSString  *sugarSyncAccessTokenLocation, *sugarSyncRefreshTokenLocation;

    RSugarSyncServiceItem *_rSugarSyncServiceItem;
    
    NSString *newFolderID;
}
@property (nonatomic, retain) RCommunication *connectCon;
@property (nonatomic, retain) NSString *workingFolder;


-(void)noNetworkMessage;

-(void)broadCastRequest:(NSURLRequest*)req withTag:(int)tag andFile:(RItem*)file;

-(void)somethingWasWrongForTag:(int)tag errorMessage:(BOOL)displayErrorMessage andFile:(RItem*)file;

-(void)handleWrongStatusPerTag:(int)tag andFile:(RItem*)file;

//-(NSString*)returnXMLForCopyFile:(fileObj*)file andDic:(NSDictionary*)dic;

@end
