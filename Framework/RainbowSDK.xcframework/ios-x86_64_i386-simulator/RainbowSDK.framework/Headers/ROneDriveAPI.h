#import "SBJsonParser.h"
#import "ROAuthAPI.h"

@interface ROneDriveAPI : ROAuthAPI
{
    BOOL userIsSignedOut;
    
    BOOL success;
    
    int workingTag;
}
-(void)signOut;
@end
