#import "RSugarSyncManager.h"

@class RIDriveServiceItem;

@interface RIDriveManager : RSugarSyncManager<NSCopying>
{
    RIDriveServiceItem *_riDriveServiceItem;
}
@property (nonatomic, retain)NSString *baseURL;

@end
