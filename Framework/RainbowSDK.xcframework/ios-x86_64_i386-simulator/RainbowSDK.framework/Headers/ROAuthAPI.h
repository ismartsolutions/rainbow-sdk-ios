#import "RAPI.h"
#import "RCommunication.h"
#import "SBJsonParser.h"
#import <WebKit/WebKit.h>

@class ROAuthService;
@class ROAuthServiceItem;


@interface ROAuthAPI : RAPI<RCommunicationDelegate, WKNavigationDelegate>
{
    ROAuthService *roAuthService;

    BOOL allowRequest;
    BOOL authorizeScreen;
}
@property (nonatomic, retain) NSURL *authorizationURL;

@property (nonatomic, retain) NSString *tokenURLString;
@property (nonatomic, retain) NSString *refreshToken;

-(id)initWithServiceItem:(ROAuthServiceItem *)ss andService:(ROAuthService*)gBO;

-(void)initialize;

-(NSMutableURLRequest*)returnRequestForGrantType:(NSString*)gType andCode:(NSString*)code;

-(void)getCredentialsForAccess;

-(void)getTokenWithCode:(NSString*)code;

- (NSData*)encodeDictionary:(NSDictionary*)dictionary;

-(void)loginIsSuccessfull;
-(void)loginHasFailed:(int)tag;

-(void)parseTokenFromDic:(NSDictionary*)dic;
-(void)informDelegate:(int)tag;
@end
