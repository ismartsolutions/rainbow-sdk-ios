#import <Foundation/Foundation.h>

extern NSString * const SSAccessToken;
extern NSString * const SSRefreshToken;
extern NSString * const SSActivationDate;

extern NSString * const BoxAccessToken;
extern NSString * const BoxRefreshToken;
extern NSString * const BoxExpirationDate;

extern NSString * const EgnyteAccessToken;
extern NSString * const EgnyteRefreshToken;
extern NSString * const EgnyteExpirationDate;
extern NSString * const EgnyteDomain;

extern NSString * const iDriveUserID;
extern NSString * const iDrivePassword;

extern NSString * const MediaFireSessionToken;
extern NSString * const MediaFireSecretKey;
extern NSString * const MediaFireTime;
extern NSString * const MediaFirePKey;

extern NSString * const ShareFileAccessToken;
extern NSString * const ShareFileRefreshToken;
extern NSString * const ShareFileExpirationDate;
extern NSString * const ShareFileSubDomain;
extern NSString * const ShareFileAppCp;

extern NSString * const SkyDriveAccessToken;
extern NSString * const SkyDriveRefreshToken;
extern NSString * const SkyDriveExpirationDate;

extern NSString * const OneDriveAccessToken;
extern NSString * const OneDriveRefreshToken;
extern NSString * const OneDriveExpirationDate;

extern NSString * const DropBoxAccessToken;
extern NSString * const DropBoxRefreshToken;
extern NSString * const DropBoxExpirationDate;
extern NSString * const DropBoxUID;

extern NSString * const DriveAccessToken;
extern NSString * const DriveRefreshToken;
extern NSString * const DriveExpirationDate;

extern NSString * const StoreServiceCredentials;

extern NSString * const SAVE_SERVICE_ACCOUNTS;
extern NSString * const UPDATE_UI_NEW_ACCOUNT;

@interface RKeys : NSObject
{
    
}
@property (nonatomic, retain) NSMutableArray *updatingMediaFireSecret;
+ (RKeys *)sharedSingleton;
+(NSArray*)dateFormatList;
+(NSString *)cacheFilePath:(NSString *)fileName;
+(NSString *)localFile:(NSString*)fileName;
+(NSString *)tempDataFilePath:(NSString *)fileName;
@end
