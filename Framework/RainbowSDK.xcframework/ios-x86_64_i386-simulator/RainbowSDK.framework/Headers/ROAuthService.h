
#import "RService.h"
#import "RAPI.h"

@class ROAuthServiceItem;

@interface ROAuthService : RService<RAPIDelegate>
{
    
}
@property (nonatomic, retain) NSString *oauthClientID, *oauthClientSecret;
@property (nonatomic, retain) NSString *scopes, *redirectURI;

-(void)refreshToken:(ROAuthServiceItem*)sObj;
@end

@interface ROAuthServiceItem : RServiceItem<NSCoding>
{
}
@property (nonatomic, retain) NSString  *oauthAccessToken, *oauthRefreshToken;
@property (nonatomic, retain) NSDate *oauthExpirationDate;

-(void)refreshToken;
@end
