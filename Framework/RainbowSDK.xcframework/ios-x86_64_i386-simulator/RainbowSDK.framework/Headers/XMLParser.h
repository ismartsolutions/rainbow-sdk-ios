#import <Foundation/Foundation.h>

@interface XMLParser : NSObject<NSXMLParserDelegate>
{
    NSData *dataToParse;
    
    BOOL isElementOfInterest;
    
    NSString *searchingElement;
    NSXMLParser *parser;
    
    BOOL listContentsOfFolder;
    
    NSMutableDictionary *data;
}
@property (nonatomic, retain) NSString *valueOfElementOfInterest;
@property (nonatomic, retain) NSMutableDictionary *valueOfElements;
@property (nonatomic, retain) NSMutableDictionary *attrOfElements;

@property (nonatomic, retain) NSMutableArray *folders, *files;
@property (nonatomic, retain) NSMutableString *foundCharacters;
@property (nonatomic, retain) NSString *quota, *usage, *username, *nickname;

-(id)initWithData:(NSData*)data findDataForElement:(NSString*)searchElement shouldListContentsOfFolder:(BOOL)list;

-(void)startParsing;
@end
