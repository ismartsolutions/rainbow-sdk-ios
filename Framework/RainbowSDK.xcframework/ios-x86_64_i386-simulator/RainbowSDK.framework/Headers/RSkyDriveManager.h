#import "RSugarSyncManager.h"
#import "SBJsonWriter.h"

@class RSkyDriveServiceItem;

@interface RSkyDriveManager : RSugarSyncManager<NSCopying>
{
    NSString  *skyDriveAccessToken, *skyDriveRefreshToken;

    RSkyDriveServiceItem *_rSkyDriveServiceItem;
    
    NSString *baseURL;
    
    long long maxFileSize;
}
@property (nonatomic) SearchTypes searchType;
@property (nonatomic, retain) NSString *searchKey;
-(NSString*)returnCorrectPath:(NSString*)path;

-(void)renameFileWithTag:(int)_tag andFile:(RItem *)file toFileName:(NSString *)fileName withFileID:(NSString*)fID;
@end
