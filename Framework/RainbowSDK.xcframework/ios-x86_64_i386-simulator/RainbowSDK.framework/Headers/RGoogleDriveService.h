#import "ROAuthService.h"

@interface RGoogleDriveService : ROAuthService<NSCoding, NSCopying>
{
    
}
@property (nonatomic, retain) NSString *scopes;
@end

@interface RGoogleDriveServiceItem : ROAuthServiceItem<NSCoding, NSCopying>
{
    
}
@end
