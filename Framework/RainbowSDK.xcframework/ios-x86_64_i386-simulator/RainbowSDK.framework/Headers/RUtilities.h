//
// Created by Vasilis Gkanis on 24/1/20.
// Copyright (c) 2020 i-SmartSolutions. All rights reserved.
//

#import <UIKit/UIkit.h>


@interface RUtilities : NSObject
{
    
}
+(NSString*)escapeString:(NSString*)input;
+(NSString*)returnXMLFromDictionary:(NSDictionary*)dic withWrapperTag:(NSString*)wrapper;
+(NSDate*)convertStringToDate:(NSString*)dS;

+(NSString*)trim:(NSString *)s;

+(void)showAlert:(NSString*)message inParentController:(UIViewController*)vc;
@end
