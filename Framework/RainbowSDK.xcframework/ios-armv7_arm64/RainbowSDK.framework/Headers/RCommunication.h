
#import <Foundation/Foundation.h>

#import "Reachability.h"

@class RItem;


@protocol RCommunicationDelegate <NSURLConnectionDelegate>

-(void)connectionFinished:(int)tag withData:(NSData*)data andTransferredFile:(RItem*)file;
-(void)connectionFailed:(int)tag andTransferredFile:(RItem*)file;

-(void)receivedLocationDictionary:(NSDictionary*)dic withStatusCode:(int)sCode withTag:(int)tag andTransferredFile:(RItem*)file;

@end

@interface RCommunication : NSObject
{
    id delegate;
    
    BOOL isRequestCanceled;
    
    BOOL checkForSSL;
}
@property (nonatomic,assign) id<RCommunicationDelegate>delegate;
@property (nonatomic, retain) NSURLConnection  *communicationCon;
@property (nonatomic, retain) NSMutableData *communicationData;
@property int tag;
@property BOOL success;
@property (nonatomic, retain) RItem *transferredFile;
@property (copy)void (^onFinished)(int tag, NSData *data);

-(id)initWithRequest:(NSURLRequest *)req;

-(void)startWithRequest:(NSURLRequest *)req;

-(void)cancelRequest;

-(void)cancelPreviousPerformrequest;
@end
