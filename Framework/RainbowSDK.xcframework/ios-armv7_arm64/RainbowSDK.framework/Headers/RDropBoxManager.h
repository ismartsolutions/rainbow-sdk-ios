#import <Foundation/Foundation.h>
#import "SBJsonWriter.h"
#import "RSugarSyncManager.h"

@class RDropBoxServiceItem;

@interface RDropBoxManager : RSugarSyncManager<NSCopying>
{
    RDropBoxServiceItem *_rDropBoxServiceItem;
    
    NSString  *dropboxAccessToken;
    NSString *baseURL;
    
    BOOL deleteFolder;
    
    long long maxFileSize;
    
    NSMutableArray *_folders, *_files;
    
    RAccount *rAccount;
}
@property (nonatomic, retain) NSMutableDictionary *quota;
          
@end
