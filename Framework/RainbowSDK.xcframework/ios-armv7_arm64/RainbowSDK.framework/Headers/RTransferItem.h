#import <Foundation/Foundation.h>

@class RServiceItem;

@interface RTransferItem : NSObject<NSCopying>
{
    
}
@property (nonatomic, retain)NSString *filePath, *method, *workingServiceName;
@property (nonatomic, retain) NSDictionary *headers;
@property BOOL hasBody;
@property (nonatomic, retain) NSData *bodyPrefix, *bodySuffix,*bodyMain;
@property (nonatomic, retain) RServiceItem *workingCredentialObj;
@property (nonatomic, retain) NSURL *url;
@property long long cachedSize;
@property long long totalUploadedSize, uploadedPerSessionSize;
@property (nonatomic, retain) NSString *sessionID;
@property BOOL cachedFileIsUploaded;
@property (nonatomic, retain) NSString *uploadPath;
@end
