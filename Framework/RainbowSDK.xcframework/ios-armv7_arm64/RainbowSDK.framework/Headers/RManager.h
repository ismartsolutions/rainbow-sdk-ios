#import <Foundation/Foundation.h>
#import "RItem.h"
#import "RAccount.h"

@class RServiceItem;
@class RTransferItem;
@class Rainbow;

@protocol RManagerDelegate <NSObject>

@required
-(void)filesListIsReady;
-(void)somethingWasWrong;

-(void)ioTaskCompletedWithResponse:(NSString*)newID forAction:(CloudManagerActions)action;

-(void)dismissAlertView;

@end

@protocol RManagerTransferDelegate <NSObject>
@required

-(void)onFileIsReadyToBeUploaded:(RItem*)file withSyncObj:(RTransferItem*)upObj andStatus:(BOOL)status;
@end


@interface RManager : NSObject<NSCopying>
{
    id delegate, transferDelegate;
    
    NSString *searchingPath;
    
    Rainbow *rainbow;
}
@property (nonatomic, retain) RServiceItem *serviceItem;
@property (nonatomic, assign) id<RManagerDelegate>delegate;
@property (nonatomic, assign) id<RManagerTransferDelegate>transferDelegate;
@property (nonatomic, retain) NSMutableArray *availableFiles;
@property (nonatomic, retain) NSString *serviceName, *serviceID;
@property (nonatomic) void (^onFileToBeDownloaded)(RTransferItem *downObj) ;
@property BOOL canShare, canAddNewItem, downloadImageForPreview, canMarkFavorite, canRename;

@property (nonatomic) void (^onAccount)(RAccount *rAccount) ;

-(id)initWithServiceItem:(RServiceItem*)gObj;

-(void)initialize;

-(NSDictionary*)returnCredentialsForFile:(RItem*)file;

-(RServiceItem*)returnServiceItem;
-(NSString*)returnServiceName;

-(void)createFilesListWithFolderList:(NSMutableArray*)folders andFilesList:(NSMutableArray*)files;

-(void)setSearchingPath:(NSString*)_sPath;
-(NSString*)returnSearchingPath;

-(BOOL)acceptSelectedFolder;

-(void)empty;
-(void)getFilesInFolder:(NSString*)path;
-(void)searchItemsInFolder:(NSString*)path forKey:(NSString*)key forType:(SearchTypes)type;

-(NSString*)getDate:(NSDate*)date;
-(NSDate*)convertStringToDate:(NSString*)inputDate;

-(void)downloadFile:(RItem*)file;
-(void)downloadFileForView:(RItem*)file;

-(void)uploadLocalFile:(RItem*)file toPath:(NSString*)path;
-(void)startSessionFile:(RItem*)file toPath:(NSString*)path;
-(NSMutableURLRequest*)appendSession:(NSString*)sessionID andOffset:(long long)offset;
-(NSMutableURLRequest*)closeSession:(NSString*)sessionID andOffset:(long long)offset forFile:(RItem*)file atPath:(NSString*)path;

-(NSMutableURLRequest*)nextChunkForURL:(NSURL*)url andOffset:(long long)offset andTotalSize:(long long)totalSize;

-(void)copyFile:(RItem*)file toPath:(NSString*)path;
-(void)moveFile:(RItem*)file toPath:(NSString*)path;
-(void)deleteFile:(RItem*)file;

-(void)renameFile:(RItem*)file toFileName:(NSString*)fileName;
-(void)createFolder:(NSString*)folderName atPath:(NSString*)parentFolder;
-(void)deleteFolder:(RItem*)file;

-(void)deleteLocalFileAtPath:(NSString*)path;

-(NSString*)escapeString:(NSString*)input;
- (NSData*)encodeDictionary:(NSDictionary*)dictionary;

-(void)getAccount;

-(NSArray*)extractItemPathURLFromResponse:(NSString*)response;

@end


