#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "SBJsonParser.h"

@class RItem;

@protocol RSyncTransferHelperDelegate <NSObject>
-(void)filesProgressStatus:(NSDictionary*)dic;

@end

@interface RTransferHelper : NSObject<NSStreamDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>
{
    long maxDownBuffer, minDownBuffer;
    RItem *transferredFile;
    
    //NSURLSessionDataTask *downloadDataTask;
    NSURLConnection *downloadCon;
    
    BOOL onlyDownload;
    
    NSArray *conArray;
    
    BOOL success;
    
    NSInputStream *uploadStream;
    NSOutputStream *outputStream, *writeStream;
    NSFileHandle *downHandle;
    BOOL isFileDown;
    
    BOOL shouldClose;
    
    int downFromMegaStatus;
    
    BOOL writingBytesToOutPutStream;
    
    BOOL isLoggedin;
    
    BOOL checkForAvailSpaceOnce;
    
    id delegate;
    
    NSURL *bookmarkFileURL;
    
    long long totalBytesToBeUploaded, bytesThatAreUploaded;
    long long actualTotalBytesToBeUploaded;
}
@property (nonatomic, assign) id<RSyncTransferHelperDelegate>delegate;
@property (nonatomic, retain) NSMutableData *downData;
@property(nonatomic, retain) NSURL *uploadSessionUrl;

@property (nonatomic) void (^onSuccess)(RItem *workingFileObj, NSString *error) ;
@property (nonatomic) void (^onSuccessGetResponse)(RItem *workingFileObj, NSString *response, NSString *error) ;
-(id)initWithFile:(RItem*)_File andObj:(NSArray*)objArrays;
-(void)start;
-(void)cancelRequest;

-(void)smartCloudStatus:(NSString *)errorDscription;
-(void)updateProgressForSmartCloud:(long long)progress;
@end
