#import <Foundation/Foundation.h>
#import "SBJsonWriter.h"
#import "RSugarSyncManager.h"

@class RGoogleDriveServiceItem;

@interface RGoogleDriveManager :RSugarSyncManager<NSCopying>
{
    NSString  *driveAccessToken, *driveRefreshToken;

    RGoogleDriveServiceItem *_rGoogleDriveServiceItem;
    
    NSString *baseURL;
    
    NSMutableArray *_folders, *_files;

}

//-(void)getFilesInDriveFolder:(GTLDriveFileList*)fileList;


@end



