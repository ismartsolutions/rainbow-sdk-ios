
#import "RService.h"

@interface RMediaFireService : RService<NSCoding, NSCopying>
{
    
}
@property (nonatomic, retain) NSString *appID, *appKey;
@end

@interface RMediaFireServiceItem : RServiceItem<NSCoding, NSCopying>
{
    
}
@property (nonatomic, retain) NSString *sessionToken, *pkey, *time;
@property long long secretKey;
@end
