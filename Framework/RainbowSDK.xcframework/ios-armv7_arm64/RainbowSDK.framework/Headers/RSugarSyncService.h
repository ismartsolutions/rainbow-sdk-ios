#import <Foundation/Foundation.h>
#import "RService.h"
#import "RAPI.h"

@interface RSugarSyncService : RService<NSCoding, RAPIDelegate, NSCopying>
{

}
@property (nonatomic, retain) NSString *sugarSyncKey, *sugarSyncPrivateKey, *sugarSyncAppID;
@end

@interface RSugarSyncServiceItem : RServiceItem<NSCoding, RAPIDelegate, NSCopying>
{
   // progressDialog *myAlert;
}
@property (nonatomic, retain) NSString  *sugarSyncAccessTokenLocation, *sugarSyncRefreshTokenLocation;
@property (nonatomic, retain) NSDate *sugarSyncActivationDate;
@property BOOL isSugarSyncRefreshTokenAvailable;
@end
