
#import "RSugarSyncManager.h"
#import "SBJsonWriter.h"

@class ROneDriveServiceItem;

@interface ROneDriveManager : RSugarSyncManager<NSCopying>
{
    NSString  *oneDriveAccessToken, *oneDriveRefreshToken;

    ROneDriveServiceItem *_rOneDriveServiceItem;
    
    NSString *baseURL;
    
    long long maxFileSize;
}
@property (nonatomic) SearchTypes searchType;
@property (nonatomic, retain) NSString *searchKey;
-(NSString*)returnCorrectPath:(NSString*)path;

-(void)renameFileWithTag:(int)_tag andFile:(RItem *)file toFileName:(NSString *)fileName withFileID:(NSString*)fID;@end
