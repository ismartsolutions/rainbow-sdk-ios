#import "ROAuthAPI.h"

@interface RSkyDriveAPI : ROAuthAPI
{
    BOOL userIsSignedOut;
    
    BOOL success;
}

-(void)signOut;
@end
