#import <Foundation/Foundation.h>
#import "REnums.h"
#import "RItem.h"

@interface RTransferProgress : NSObject
{
    
}
@property TaskTransferActions action;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *error;
@property (nonatomic, retain) RItem *file;
@property NSUInteger total, progress;
@end

