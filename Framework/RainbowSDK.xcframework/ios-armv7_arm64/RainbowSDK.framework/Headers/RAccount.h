#import <Foundation/Foundation.h>
#import "RQuota.h"
@interface RAccount : NSObject
{
    
}
@property (nonatomic, retain) NSString *accountID;
@property (nonatomic, retain) NSString *name, *email;
@property (nonatomic, retain) RQuota *quota;
@end

