#import "ROAuthAPI.h"
#import "AppAuth.h"

@class RGoogleDriveService;
@interface RGoogleDriveAPI : ROAuthAPI
{
    
}
@property(nonatomic, retain) id<OIDExternalUserAgentSession> currentAuthorizationFlow;
-(void)connectToGoogle:(RGoogleDriveService*)service withName:(NSString*)name presentingViewController:(UIViewController*)vc andResponseBlock:(void (^)(NSString *error))block;
@end
