#import <Foundation/Foundation.h>
#import "SBJsonWriter.h"
#import "RSugarSyncManager.h"

@class RBoxServiceItem;

@interface RBoxManager : RSugarSyncManager<NSCopying>
{
    NSString  *boxAccessToken, *boxRefreshToken;

    RBoxServiceItem *_rBoxServiceItem;
    
    NSString *baseURL;
}
@end
