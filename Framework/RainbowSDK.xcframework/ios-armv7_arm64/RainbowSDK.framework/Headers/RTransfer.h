#import <Foundation/Foundation.h>
#import "RTransferHelper.h"
#import "RManager.h"
#import "RTransferProgress.h"

@interface RTransfer: NSObject<RManagerTransferDelegate, RSyncTransferHelperDelegate, RManagerDelegate>
{
}
@property (nonatomic) void (^isCompleted)(RTransfer *owner, NSString *error);
@property (nonatomic) void (^progressBlock)(RTransferProgress *progress);

/**
The only method to create an instance of this class. This class is used to transfer files from one cloud manager to another
@param upPath Where the file is uploaded to. This is usually equal to RItem.path. When we want to download a file locally, for preview, this is empty string
@param sManager The source Cloud Manager. The files will be copied from this cloud manager
@param dManager The destination Cloud Manager. The files will be copied to this cloud manager. If this manager is the RLocalManager, then the upPath is not used
*/
-(id)initWithUploadPath:(NSString *)upPath andSourceManager:(RManager *)sManager andDestinationManager:(RManager *)dManager andFileList:(NSArray*)fileLists;
-(void)start;
@end
