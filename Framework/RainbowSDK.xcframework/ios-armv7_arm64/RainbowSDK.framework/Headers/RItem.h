#import <Foundation/Foundation.h>
#import "REnums.h"

// This class represents an item (folder or file), and it has all the information that is needed to navigate to that item or download it

@interface RItem : NSObject<NSCopying, NSCoding>

// name: The Name of the item
// size: A string representation of the size (only for files)
// path: It is not really a path, but an id. We use it when we want to navigate to it or to upload a file to it
// downloadUrl: In most cases this is the same with the path. But, when we want to download a file we need to use the downloadUrl and not the path
@property (nonatomic, retain) NSString *name, *size, *path, *downloadUrl;

//uploadName: By default this is equal to the name. When we upload a file, the name of that file is determined by this variable and not the name
@property (nonatomic, retain) NSString *uploadName;

// isFile: A flag indicating if this item is a file or not
@property BOOL isFile;

// actualSize: Available only for files. The size of file in bytes
@property long long actualSize;

// localFileName: For internal use
@property (nonatomic, retain) NSString *localFileName;

//actionToBePerformed: What action are we going to perform to that file: Copy (default) or Delete
@property ItemsActions actionToBePerformed;

// if the next variables are true, then that date is available
@property BOOL isDateCreatedAvail, isDateModifiedAvail;
@property (nonatomic, retain) NSDate *dateCreated, *dateModified;

//fileIsAvailable: This is always true. It is given here as a tool to hide some items from the user
@property BOOL fileIsAvailable;

// internal use
-(NSString*)returnGuidForLocalFile;

//reasonFileIsNotAvailable: if a file is not available, then this method will let the user learn the reason that caused this file to not be available
-(NSString*)reasonFileIsNotAvailable;
@end

