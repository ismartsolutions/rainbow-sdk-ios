#import "ROAuthService.h"

@interface RShareFileService : ROAuthService<NSCoding, NSCopying>

@end

@interface RShareFileServiceItem : ROAuthServiceItem<NSCoding, NSCopying>
@property (nonatomic, retain) NSString *subDomain, *appCp;
@end
