//
// Created by Vasilis Gkanis.
// Copyright (c) 2020 i-SmartSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    cmaGetUserInfo,
    cmaCreateFile,
    cmaCreateFolder,
    cmaRenameFile,
    cmaDeleteFolder,
    cmaDeleteFile,
    cmaGetUserInfoMore,
    cmaGetAccount,
    cmaSearchItems
}CloudManagerActions;

typedef enum
{
    iaCopy,
    iaMove,
    iaDelete,
    iaShare,
}ItemsActions;

typedef enum
{
    tsNone,
    tsPending,
    tsStart,
}TaskStatus;


typedef enum
{
    ttFile,
    ttFolder,
}TaskType;

typedef enum{
    ttaNone,
    ttaFileName,
    ttaFileProgress,
    ttaFileTransferStatus,
    ttaFolderCreate,
    ttaFilesInFolder,
    ttaFolderProgress,
    ttaFolderTransferStatus,
}TaskTransferActions;

typedef enum
{
    sLogin,
    sOauth2,
    sGoogle,
}ServiceLoginType;

typedef enum
{
    stFile,
    stFileAndContent,
}SearchTypes;

@interface REnums : NSObject
@end
