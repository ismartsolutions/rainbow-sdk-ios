#import "ROAuthService.h"

@interface RSkyDriveService : ROAuthService<NSCoding, NSCopying>
{
    
}
@property (nonatomic, retain) NSString *scopes;
@end

@interface RSkyDriveServiceItem : ROAuthServiceItem<NSCoding, NSCopying>

@end
