#import "RAPI.h"
#import "RCommunication.h"

@class RIDriveServiceItem;
@class RIDriveService;

@interface RIDriveAPI : RAPI<RCommunicationDelegate>
{
    
}
-(id)initWithServiceItem:(RIDriveServiceItem *)ss andService:(RIDriveService*)gBO;
@end

