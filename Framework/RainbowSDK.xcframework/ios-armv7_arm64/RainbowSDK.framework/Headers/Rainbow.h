//
//  Rainbow.h
//  RainbowSDK
//
//  Created by Vasilis Gkanis on 29/1/20.
//  Copyright © 2020 i-SmartSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RGoogleDriveService;

@interface Rainbow : NSObject
{

}
/**
Create a singleton instance of the Rainbow class.
*/
+ (instancetype)instance;

/**
 Set appSecret and path.
@param appSecret You get this from the site when you registered your app
@param filePath This is the path that the Rainbow app will save s few data from the connected services. If it is empty, all these data will be lost when you terminate the app, and you would have to relogin to each service.
 */
-(void)setAppSecret:(NSString *)appSecret andFilePath:(NSString*)filePath;

/**
A method to  pass the information that are required to login a user to a service
@param services The keys of that dictionary are the name of the services, and the values are a NSDictionary object with the following fields:
 For OAUTH2 services (Box, DropBox,, GoogleDrive,OneDrive, SkyDrive, ShareFile): clientid, clientsecret, scope (can be empty or missing), callback
 MediaFire: appid, appkey
 iDrive: empty dictionary
 SugarSync: appid, appkey, privatekey
 e.g
    NSDictionary *boxdic = [NSDictionary dictionaryWithObjectsAndKeys:@"demo_client_id",@"clientid,@"demo_secret", @"clientsecret",@"",@"scope",@"demo_url",@"callback",nil];
    NSDictionary *services = [NSDictionary dictionaryWithObject:boxdic forKey:@"Box"];
*/
-(void)addServiceKeys:(NSDictionary*)services;

/**
 A dictionary with the available services. The key of that dictionary is the name of the service, and the value is an RService object
 In addition to all data that were given in the previous method, that object has the following variables
 name:it is a name of the Service
 list: Array with objects of type RServiceItem. An RServiceItem object is created every time a user logins to a service. Thus, this variables gives the total accounts that a user has already logged in
 connectedList: Array with objects of type RServiceItem. Total accounts that a user has already logged in and are still active (have a valid token, and thus they can be used)
 connected: It is a flag that denotes that at least one account is active
 loginType: sLogin: we need to give user password and email to login to that service
        : sOauth2, the user enters the email and password at service's website. Once login is successfully we get an access and a refresh token
*/
-(NSDictionary*)getServices;

// the next two methods are used to connect to a Google Drive account
-(void)connectToGoogle:(RGoogleDriveService*)service withName:(NSString*)name presentingViewController:(UIViewController*)vc andResponseBlock:(void (^)(NSString *error))block;
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options;

-(BOOL)libraryStatus;
@end

