#import "ROAuthService.h"

@interface ROneDriveService : ROAuthService<NSCoding, NSCopying>
{
    
}
@end

@interface ROneDriveServiceItem : ROAuthServiceItem<NSCoding, NSCopying>
{
    
}

@end
