#import "RAPI.h"
#import "RCommunication.h"

@class XMLParser;
@class RSugarSyncServiceItem;
@class RSugarSyncService;
typedef enum
{
    tRefresh,
    tAccess,
}token;


@interface RSugarSyncAPI : RAPI<RCommunicationDelegate>
{
}
-(id)initWithServiceItem:(RSugarSyncServiceItem *)ss andService:(RSugarSyncService*)gBO;

-(void)getCredentialsForAccess;
@end
