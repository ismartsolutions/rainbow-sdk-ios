#import "ROAuthService.h"

@interface RDropBoxService : ROAuthService<NSCoding, NSCopying>
{
    
}
@end


@interface RDropBoxServiceItem : ROAuthServiceItem<NSCoding, NSCopying>
@property (nonatomic, retain) NSString *uid;
@end

