#import "AESCrypt.h"
#import "RService.h"

@interface RIDriveService : RService<NSCoding, NSCopying>

@end

@interface RIDriveServiceItem : RServiceItem<NSCoding, NSCopying>
{
    
}
@property (nonatomic, retain) NSString *userID, *password;
@property (nonatomic, retain)NSString *baseURL;
@property BOOL isAccountvalid;

-(NSString*)getBody:(NSString*)extraTerm;
@end
